import { Component, OnInit } from '@angular/core';
import { DatabaseConnectionService } from './modules/core/database/database-connection.service';
import { UsuarioDAO } from './modules/core/database/usuario.dao.service';
import { CombustivelDAO } from './modules/core/database/combustivel.dao.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'desafio-front';

  constructor(
    private databaseConnectionService: DatabaseConnectionService,
    private usuarioDAO: UsuarioDAO,
    private combustivelDAO: CombustivelDAO
  ) { }

  ngOnInit() {
    try {
      this.databaseConnectionService.criarDatabase();
      this.usuarioDAO.criarTabelas();
      this.combustivelDAO.criarTabelas();
    } catch (error) {
      console.error(error);
    }
  }
}
