import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TelaInicialComponent } from './components/tela-inicial/tela-inicial.component';

const routes: Routes = [
  {
    path: 'inicio',
    component: TelaInicialComponent
  },
  {
    path: 'usuarios',
    loadChildren: './modules/usuarios/usuarios.module#UsuariosModule'
  },
  {
    path: 'combustiveis',
    loadChildren: './modules/combustiveis/combustiveis.module#CombustiveisModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
