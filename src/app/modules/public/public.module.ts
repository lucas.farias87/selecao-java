import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { TelaInicialComponent } from './components/tela-inicial/tela-inicial.component';
import { UsuariosModule } from './modules/usuarios/usuarios.module';
import { CombustiveisModule } from './modules/combustiveis/combustiveis.module';

@NgModule({
  declarations: [TelaInicialComponent],
  imports: [
    CommonModule,
    PublicRoutingModule,
    UsuariosModule,
    CombustiveisModule
  ]
})
export class PublicModule { }
