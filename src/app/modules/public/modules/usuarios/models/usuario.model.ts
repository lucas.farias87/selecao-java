export interface UsuarioModel {
    id?: number;
    nome: string;
    telefone: string;
    email: string;
}
