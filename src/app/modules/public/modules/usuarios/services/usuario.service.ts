import { Injectable } from '@angular/core';
import { UsuarioModel } from '../models/usuario.model';
import { ApiUsuarioService } from 'src/app/modules/core/api-usuario/api-usuario.service';
import { Observable } from 'rxjs';
import { RetornoPadraoModel } from 'src/app/modules/shared/models/retorno-padrao.model';

@Injectable()
export class UsuarioService {

  constructor(
    private apiUsuarioService: ApiUsuarioService
  ) { }

  cadastrarUsuario(usuario: UsuarioModel): Observable<RetornoPadraoModel<number>> {
    return this.apiUsuarioService.post(usuario);
  }

  editarUsuario(usuario: UsuarioModel): Observable<RetornoPadraoModel<number>> {
    return this.apiUsuarioService.put(usuario);
  }

  removerUsuario(id: number): Observable<RetornoPadraoModel<number>> {
    return this.apiUsuarioService.delete(id);
  }

  getUsuarios(): Observable<RetornoPadraoModel<UsuarioModel[]>> {
    return this.apiUsuarioService.getAll();
  }

  getUsuarioById(id: number): Observable<RetornoPadraoModel<UsuarioModel>> {
    return this.apiUsuarioService.getById(id);
  }
}
