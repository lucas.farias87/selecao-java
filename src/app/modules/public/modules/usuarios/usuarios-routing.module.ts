import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormUsuarioComponent } from './components/form-usuario/form-usuario.component';
import { PesquisarUsuarioComponent } from './components/pesquisar-usuario/pesquisar-usuario.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'pesquisar',
    pathMatch: 'full'
  },
  {
    path: 'cadastrar',
    component: FormUsuarioComponent
  },
  {
    path: 'editar/:id',
    component: FormUsuarioComponent
  },
  {
    path: 'pesquisar',
    component: PesquisarUsuarioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
