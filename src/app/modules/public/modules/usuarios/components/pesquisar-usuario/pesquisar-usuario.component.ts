import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { UsuarioModel } from '../../models/usuario.model';
import { MatDialog } from '@angular/material';
import { ModalDialogComponent } from 'src/app/modules/shared/components/modal-dialog/modal-dialog.component';
import { MensagemEnum } from 'src/app/modules/shared/models/mensagem.enum';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-pesquisar-usuario',
  templateUrl: './pesquisar-usuario.component.html',
  styleUrls: ['./pesquisar-usuario.component.css']
})
export class PesquisarUsuarioComponent implements OnInit {

  displayedColumns: string[] = ['nome', 'telefone', 'email', 'acao'];
  usuarios: UsuarioModel[];

  constructor(
    private usuarioService: UsuarioService,
    private dialog: MatDialog,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    this.recuperarUsuarios();
  }

  openDialog(id: number): void {
    const dialogRef = this.dialog.open(ModalDialogComponent, {
      width: '250px',
      data: {
        titulo: MensagemEnum.TITULO_MODAL_CONFIRMACAO,
        texto: MensagemEnum.TEXTO_CONFIRMAR_REMOCAO_USUARIO
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.removerUsuario(id);
      }
    });
  }

  recuperarUsuarios() {
    this.usuarioService.getUsuarios().
      subscribe(
        (response) => {
          this.usuarios = response.payload;
        }
      );
  }

  removerUsuario(id: number) {
    this.usuarioService.removerUsuario(id).
      subscribe(
        (response) => {
          this.toastrService.success(response.mensagem);
          this.recuperarUsuarios();
        },
        (error) => {
          this.toastrService.error(error.mensagem);
        }
      );
  }

}
