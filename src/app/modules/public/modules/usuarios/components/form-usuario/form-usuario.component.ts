import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { getMensagemInputErro } from 'src/app/modules/shared/functions/msg-input-erro.function';
import { ConstantsUtil } from 'src/app/modules/shared/util/constants-util.const';
import { UsuarioService } from '../../services/usuario.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-form-usuario',
  templateUrl: './form-usuario.component.html',
  styleUrls: ['./form-usuario.component.css']
})
export class FormUsuarioComponent implements OnInit {

  formUsuario: FormGroup;
  isEdicao = false;

  constructor(
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.criarForm();
    this.verificarEdicao();
  }

  verificarEdicao() {
    const idUsuario = this.recuperarIdUsuario();
    if (idUsuario && idUsuario !== null && idUsuario !== undefined) {
      this.isEdicao = true;
      this.getUsuario(idUsuario);
    }
  }

  private recuperarIdUsuario() {
    return this.activatedRoute.snapshot.params.id;
  }

  criarForm() {
    this.formUsuario = this.fb.group({
      nome: [null, Validators.required],
      telefone: [null, Validators.compose([Validators.required, Validators.maxLength(ConstantsUtil.TAMANHO_TELEFONE_COM_NOVE), Validators.minLength(ConstantsUtil.TAMANHO_TELEFONE_SEM_NOVE)])],
      email: [null, Validators.compose([Validators.required, Validators.email])],
    });
  }

  getMensagemErro(controlName: string): string {
    return getMensagemInputErro(this.formUsuario.get(controlName));
  }

  salvar() {
    this.ativarMensagensErros();
    if (this.formUsuario.valid) {
      if (this.isEdicao) {
        this.editarUsuario();
      } else {
        this.cadastrarUsuario();
      }
    }
  }

  cancelar() {
    this.formUsuario.reset();
    this.navegarParaTelaPesquisa();
  }

  navegarParaTelaPesquisa() {
    this.router.navigateByUrl('/pesquisar');
  }

  getUsuario(id: number) {
    this.usuarioService.getUsuarioById(id).subscribe(
      (response) => {
        this.formUsuario.patchValue(response.payload);
      },
      (error) => {
        this.toastrService.error(error.mensagem);
      }
    );
  }

  cadastrarUsuario() {
    this.usuarioService.cadastrarUsuario(this.formUsuario.getRawValue()).
      subscribe(
        (response) => {
          this.toastrService.success(response.mensagem);
          this.navegarParaTelaPesquisa();
        },
        (error) => {
          this.toastrService.error(error.mensagem);
        }
      );
  }

  editarUsuario() {
    this.usuarioService.editarUsuario({
      id: this.recuperarIdUsuario(),
      ...this.formUsuario.getRawValue()
    }).
      subscribe(
        (response) => {
          this.toastrService.success(response.mensagem);
          this.navegarParaTelaPesquisa();
        },
        (error) => {
          this.toastrService.error(error.mensagem);
        }
      );
  }

  private ativarMensagensErros(): void {
    Object.keys(this.formUsuario.controls).forEach(ctrl => {
      this.formUsuario.get(ctrl).updateValueAndValidity();
    });
  }

}
