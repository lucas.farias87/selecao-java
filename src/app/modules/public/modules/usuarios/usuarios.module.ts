import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { FormUsuarioComponent } from './components/form-usuario/form-usuario.component';
import { PesquisarUsuarioComponent } from './components/pesquisar-usuario/pesquisar-usuario.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { UsuarioService } from './services/usuario.service';

@NgModule({
  declarations: [
    FormUsuarioComponent,
    PesquisarUsuarioComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    UsuariosRoutingModule,
    SharedModule
  ],
  providers: [
    UsuarioService
  ]
})
export class UsuariosModule { }
