import { Injectable } from '@angular/core';
import { ApiCombustivelService } from 'src/app/modules/core/api-combustivel/api-combustivel.service';
import { Observable } from 'rxjs';
import { RetornoPadraoModel } from 'src/app/modules/shared/models/retorno-padrao.model';
import { CombustivelModel } from '../models/combustivel.model';
import { PaginacaoModel } from 'src/app/modules/shared/models/paginacao.model';

@Injectable()
export class CombustiveisService {

  constructor(
    private apiCombustivelService: ApiCombustivelService
  ) { }

  salvarArquivo(linhasArquivo: string[]): Observable<RetornoPadraoModel<number>> {
    return this.apiCombustivelService.post(linhasArquivo);
  }

  recuperarCombustiveis(pagina: number, ordenarRevendedor: boolean): Observable<RetornoPadraoModel<PaginacaoModel<CombustivelModel[]>>> {
    return this.apiCombustivelService.getAll(pagina, ordenarRevendedor);
  }

  recuperarEnderecos() {
    return this.apiCombustivelService.getAllEnderecos();
  }

  calcularMediaMunicipio(idMunicipio: number) {
    return this.apiCombustivelService.calcularMediaCombustivelPorMunicipio(idMunicipio);
  }
}
