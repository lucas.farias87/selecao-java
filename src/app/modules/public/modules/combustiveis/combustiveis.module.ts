import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CombustiveisRoutingModule } from './combustiveis-routing.module';
import { ImportarArquivosComponent } from './components/importar-arquivos/importar-arquivos.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { CombustiveisService } from './services/combustiveis.service';
import { ExibirConteudoArquivoComponent } from './components/exibir-conteudo-arquivo/exibir-conteudo-arquivo.component';
import { ModalCalcularMediaComponent } from './components/modal-calcular-media/modal-calcular-media.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ImportarArquivosComponent,
    ExibirConteudoArquivoComponent,
    ModalCalcularMediaComponent
  ],
  imports: [
    CommonModule,
    CombustiveisRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  entryComponents: [
    ModalCalcularMediaComponent
  ],
  providers: [
    CombustiveisService
  ]
})
export class CombustiveisModule { }
