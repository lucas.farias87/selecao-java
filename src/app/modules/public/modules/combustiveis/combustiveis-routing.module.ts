import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImportarArquivosComponent } from './components/importar-arquivos/importar-arquivos.component';
import { ExibirConteudoArquivoComponent } from './components/exibir-conteudo-arquivo/exibir-conteudo-arquivo.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'arquivos',
    pathMatch: 'full'
  },
  {
    path: 'arquivos',
    component: ImportarArquivosComponent
  },
  {
    path: 'visualizar',
    component: ExibirConteudoArquivoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CombustiveisRoutingModule { }
