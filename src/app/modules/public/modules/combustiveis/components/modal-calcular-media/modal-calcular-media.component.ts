import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { getMensagemInputErro } from 'src/app/modules/shared/functions/msg-input-erro.function';
import { EnderecoModel } from '../../models/endereco.model';
import { CombustiveisService } from '../../services/combustiveis.service';

@Component({
  selector: 'app-modal-calcular-media',
  templateUrl: './modal-calcular-media.component.html',
  styleUrls: ['./modal-calcular-media.component.css']
})
export class ModalCalcularMediaComponent implements OnInit {

  form: FormGroup;
  enderecos: EnderecoModel[];
  media: number;

  constructor(
    public dialogRef: MatDialogRef<ModalCalcularMediaComponent>,
    private fb: FormBuilder,
    private combustiveisService: CombustiveisService
  ) { }

  ngOnInit() {
    this.criarForm();
    this.recuperarEnderecos();
  }

  recuperarEnderecos() {
    this.combustiveisService.recuperarEnderecos().
      subscribe(
        (response) => {
          this.enderecos = response.payload;
        }
      );
  }

  calcularMedia() {
    this.combustiveisService.calcularMediaMunicipio(this.form.getRawValue().municipio).
      subscribe(
        (response) => {
          this.media = response.payload;
        }
      );
  }

  criarForm() {
    this.form = this.fb.group({
      municipio: [null, Validators.required]
    });
  }

  getMensagemErro(controlName: string): string {
    return getMensagemInputErro(this.form.get(controlName));
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
