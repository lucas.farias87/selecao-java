import { Component, OnInit } from '@angular/core';
import { CombustivelModel } from '../../models/combustivel.model';
import { CombustiveisService } from '../../services/combustiveis.service';
import { PaginacaoModel } from 'src/app/modules/shared/models/paginacao.model';
import { PageEvent, MatDialog } from '@angular/material';
import { ModalCalcularMediaComponent } from '../modal-calcular-media/modal-calcular-media.component';

@Component({
  selector: 'app-exibir-conteudo-arquivo',
  templateUrl: './exibir-conteudo-arquivo.component.html',
  styleUrls: ['./exibir-conteudo-arquivo.component.css']
})
export class ExibirConteudoArquivoComponent implements OnInit {

  displayedColumns: string[] = ['regiao', 'sigla', 'municipio', 'revendedor', 'codigo', 'produto', 'data', 'valorCompra', 'valorVenda', 'unidadeMedida', 'bandeira'];
  combustiveis: PaginacaoModel<CombustivelModel[]>;
  ordenarRevendedor = false;

  constructor(
    private combustiveisService: CombustiveisService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.recuperarCombustiveis(0);
  }

  recuperarCombustiveis(pagina: number) {
    this.combustiveisService.recuperarCombustiveis(pagina, this.ordenarRevendedor).
      subscribe(
        (response) => {
          this.combustiveis = response.payload;
        }
      );
  }

  ordenarPorRevendedor() {
    this.ordenarRevendedor = true;
    this.recuperarCombustiveis(0);
  }

  paginar(event: PageEvent) {
    this.recuperarCombustiveis(event.pageIndex + 1);
  }

  openDialog(): void {
    this.dialog.open(ModalCalcularMediaComponent, {
      width: '500px'
    });
  }

}
