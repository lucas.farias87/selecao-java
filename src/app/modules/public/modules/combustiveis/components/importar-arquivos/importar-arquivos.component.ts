import { Component, OnInit } from '@angular/core';
import { CombustiveisService } from '../../services/combustiveis.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-importar-arquivos',
  templateUrl: './importar-arquivos.component.html',
  styleUrls: ['./importar-arquivos.component.css']
})
export class ImportarArquivosComponent implements OnInit {

  leitorDeCSV = new FileReader();
  linhasArquivo: string[];
  habilitarSalvar = false;

  constructor(
    private combustiveisService: CombustiveisService,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    this.leitorDeCSV.onload = () => this.leCSV();
  }

  pegaCSV(inputFile) {
    const file = inputFile.target.files[0];
    this.leitorDeCSV.readAsText(file, 'ISO-8859-1');
  }

  leCSV() {
    const linhasArquivos = this.leitorDeCSV.result.toString().split('\n');
    linhasArquivos.splice(0, 1);
    this.linhasArquivo = linhasArquivos;
    this.habilitarSalvar = true;
  }

  salvarArquivo() {
    if (this.linhasArquivo && this.linhasArquivo.length) {
      this.combustiveisService.salvarArquivo(this.linhasArquivo).
        subscribe(
          (response) => {
            this.toastrService.success(response.mensagem);
          },
          (error) => {
            this.toastrService.error(error.mensagem);
          }
        );
    }
  }

}
