import { EnderecoModel } from './endereco.model';
import { RevendedorModel } from './revendedor.model';
import { ProdutoModel } from './produto.model';
import { BandeiraModel } from './bandeira.model';

export interface CombustivelModel {
    id?: number;
    endereco: EnderecoModel | number;
    revendedor: RevendedorModel | number;
    codigo: string;
    produto: ProdutoModel | number;
    data: string;
    valorCompra: string;
    valorVenda: string;
    unidadeMedida: string;
    bandeira: BandeiraModel | number;
}
