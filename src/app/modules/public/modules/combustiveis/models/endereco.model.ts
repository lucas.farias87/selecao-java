export interface EnderecoModel {
    id?: number;
    regiao: string;
    sigla: string;
    municipio: string;
}
