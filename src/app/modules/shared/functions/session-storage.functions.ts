import { SessionStorageKeyEnum, TipoStorageEnum } from '../models/session-storage.enum';

export function recuperarValor<T>(chave: SessionStorageKeyEnum | string, tipoStorage: TipoStorageEnum = TipoStorageEnum.SESSAO): T {
    const item = tipoStorage === TipoStorageEnum.SESSAO ? sessionStorage.getItem(chave) : localStorage.getItem(chave);
    if (item) {
        let valor;
        try {
            valor = JSON.parse(atob(item));
        } catch (error) {
            valor = atob(item);
        }
        return valor;
    }
}

export function atribuirValor<T>(chave: SessionStorageKeyEnum | string, objeto: T, tipoStorage: TipoStorageEnum = TipoStorageEnum.SESSAO) {
    if (chave && objeto) {
        const valor = btoa(typeof objeto === 'object' ? JSON.stringify(objeto) : String(objeto));
        tipoStorage === TipoStorageEnum.SESSAO ?
            sessionStorage.setItem(chave, valor) :
            localStorage.setItem(chave, valor);
    }
}

export function limparValores(chave: SessionStorageKeyEnum[] = [], tipoStorage: TipoStorageEnum = TipoStorageEnum.SESSAO) {
    chave.forEach(key => tipoStorage === TipoStorageEnum.SESSAO ? sessionStorage.removeItem(key) : localStorage.removeItem(key));
}

export function limparSessao(tipoStorage?: TipoStorageEnum) {
    if (!tipoStorage) { sessionStorage.clear(); localStorage.clear(); }
    tipoStorage === TipoStorageEnum.SESSAO ? sessionStorage.clear() : localStorage.clear();
}
