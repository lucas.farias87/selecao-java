import { MensagemInputErro } from '../models/msg-input-erro.const';
import { AbstractControl } from '@angular/forms';

export function getMensagemInputErro(control: AbstractControl): string {
    const errorName = Object.keys(control.errors)[0];
    switch (errorName) {
        case 'minlength':
          return MensagemInputErro[errorName].replace('{VALUE}', (control.errors.minlength.requiredLength || 'NOT_CONTENT'));
        case 'maxlength':
          return MensagemInputErro[errorName].replace('{VALUE}', (control.errors.maxlength.requiredLength || 'NOT_CONTENT'));
        default:
          return MensagemInputErro[errorName];
      }
}
