export interface RetornoPadraoModel<T> {
    mensagem?: string;
    payload: T;
}
