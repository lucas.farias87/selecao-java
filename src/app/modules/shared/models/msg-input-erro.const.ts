export const MensagemInputErro = {
    'phone':
        'Telefone inválido',
    'required':
        'Este campo é obrigatório',
    'pattern':
        'Campo inválido.',
    'email':
        'E-mail inválido.',
    'minlength':
        'Informe no mínimo {VALUE} caracteres.',
    'maxlength':
        'Informe no máximo {VALUE} caracteres.',
}