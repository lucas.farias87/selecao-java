export interface PaginacaoModel<T> {
    itens: T;
    pagina: number;
    totalItens: number;
}
