export interface UsuarioAutenticadoModel {
    tipo: string;
    token: string;
}
