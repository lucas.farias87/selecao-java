export enum MensagemEnum {
    DADOS_SALVO_COM_SUCESSO = 'Dados salvo com sucesso!',
    DADOS_REMOVIDOS_COM_SUCESSO = 'Dados removidos com sucesso!',
    NENHUM_RESULTADO_ENCONTRADO = 'Nenhum resultado encontrado.',
    ERRO_CRIACAO_DATABASE = 'Erro ao criar o banco de dados',
    ERRO_CADASTRAR = 'Erro ao cadastrar {0}',
    ERRO_EDITAR = 'Erro ao editar {0}',
    ERRO_REMOVER = 'Erro ao remover {0}',
    ERRO_CONSULTAR_BASE = 'Erro ao consular a base',
    TITULO_MODAL_CONFIRMACAO = 'Tem certeza?',
    TEXTO_CONFIRMAR_REMOCAO_USUARIO = 'Deseja mesmo remover este usuário?',
}
