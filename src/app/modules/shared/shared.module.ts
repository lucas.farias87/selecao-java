import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { MaterialModule } from './modules/material/material.module';
import { RouterModule } from '@angular/router';
import { DirectivesModule } from './directives/directives.module';
import { PipesModule } from './pipes/pipes.module';
import { ToastrModule } from 'ngx-toastr';
import { ModalDialogComponent } from './components/modal-dialog/modal-dialog.component';

@NgModule({
  declarations: [
    HeaderComponent,
    ModalDialogComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    DirectivesModule,
    PipesModule,
    RouterModule,
    ToastrModule.forRoot()
  ],
  entryComponents: [
    ModalDialogComponent
  ],
  exports: [
    MaterialModule,
    DirectivesModule,
    PipesModule,
    HeaderComponent,
    ModalDialogComponent
  ]
})
export class SharedModule { }
