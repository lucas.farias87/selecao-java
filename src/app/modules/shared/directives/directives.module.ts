import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TelefoneDirective } from './mascaras/telefone.directive';

@NgModule({
  declarations: [
    TelefoneDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TelefoneDirective
  ]
})
export class DirectivesModule { }
