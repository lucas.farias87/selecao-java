import { Directive, forwardRef, ElementRef, HostListener } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import * as StringMask from 'string-mask';
import { ConstantsUtil } from '../../util/constants-util.const';

@Directive({
  selector: '[appTelefone]',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TelefoneDirective),
    multi: true
  }]
})
export class TelefoneDirective implements ControlValueAccessor {

  onTouched: any;
  onChange: any;
  value: any;
  private regexApplyTelefone: StringMask;
  private regexApplyTelefoneDigito: StringMask;
  private digitoBackspace = 8;

  constructor(private element: ElementRef) {
    this.regexApplyTelefone = new StringMask('(00) 0000-0000');
    this.regexApplyTelefoneDigito = new StringMask('(00) 00000-0000');
  }

  writeValue(value: any): void {
    if (value != null) {
      if (value.indexOf('-') === -1) {
        if (value.length === ConstantsUtil) {
          this.element.nativeElement.value = this.regexApplyTelefone.apply(value);
        } else if (value.length === ConstantsUtil.TAMANHO_TELEFONE_COM_NOVE) {
          this.element.nativeElement.value = this.regexApplyTelefoneDigito.apply(value);
        }
      }
    }
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  private getValorAbsoluto(targetValue: string): string {
    return targetValue.replace(/\D/g, '');
  }

  @HostListener('keyup', ['$event'])
  onKeyup($event: any) {
    const valor = this.getValorAbsoluto($event.target.value);
    if ($event.keyCode === this.digitoBackspace) {
      if (valor.length === ConstantsUtil.TAMANHO_TELEFONE_SEM_NOVE) {
        this.aplicarMascara($event);
      } else {
        this.onChange(valor);
        return;
      }
    }
    this.aplicarMascara($event);
  }

  @HostListener('keydown', ['$event'])
  onKeydown($event: any) {
    const valor = this.getValorAbsoluto($event.target.value);
    if ($event.keyCode === this.digitoBackspace) {
      if (valor.length === ConstantsUtil.TAMANHO_TELEFONE_SEM_NOVE) {
        this.aplicarMascara($event);
      } else {
        this.onChange(valor);
        return;
      }
    }
    this.aplicarMascara($event);
  }

  aplicarMascara($event: any) {
    const valor = this.getValorAbsoluto($event.target.value);

    if (valor.length <= ConstantsUtil.TAMANHO_TELEFONE_SEM_NOVE) {
      this.onChange(valor);
      $event.target.value = this.regexApplyTelefone.apply(valor);
    } else if (valor.length <= ConstantsUtil.TAMANHO_TELEFONE_COM_NOVE) {
      this.onChange(valor);
      $event.target.value = this.regexApplyTelefoneDigito.apply(valor);
    } else {
      this.onChange(valor);
    }
    return;
  }

  setDisabledState(isDisabled: boolean): void {
    if (this.element != null) {
      this.element.nativeElement.disabled = isDisabled;
    }
  }

}
