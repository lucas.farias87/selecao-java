import { Injectable } from '@angular/core';
import { recuperarValor, atribuirValor } from '../../shared/functions/session-storage.functions';
import { SessionStorageKeyEnum, TipoStorageEnum } from '../../shared/models/session-storage.enum';
import { UsuarioAutenticadoModel } from '../../shared/models/usuario-autenticado.model';

@Injectable()
export class AuthenticationService {

  constructor() { }

  isLogged() {
    return !!recuperarValor(SessionStorageKeyEnum.USUARIO_LOGADO);
  }

  getUsuarioLogado(): UsuarioAutenticadoModel {
    const usuario = recuperarValor<UsuarioAutenticadoModel>(SessionStorageKeyEnum.USUARIO_LOGADO);
    if (usuario) {
      // usuario.tokenModelo = jwtDecode(usuario.token);
    }
    return usuario;
  }

  setUsuarioAutenticado(usuarioAutenticado: UsuarioAutenticadoModel, remember: boolean): void {
    atribuirValor<UsuarioAutenticadoModel>(SessionStorageKeyEnum.USUARIO_LOGADO, usuarioAutenticado,
      remember ? TipoStorageEnum.LOCAL : TipoStorageEnum.SESSAO);
  }
}
