import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { ConstantsUtil } from '../../shared/util/constants-util.const';
import { CombustivelDAO } from '../database/combustivel.dao.service';
import { CombustivelModel } from '../../public/modules/combustiveis/models/combustivel.model';
import { PaginacaoModel } from '../../shared/models/paginacao.model';
import { RetornoPadraoModel } from '../../shared/models/retorno-padrao.model';

@Injectable()
export class ApiCombustivelService {

  idEndereco: number;
  idRevendedor: number;
  idProduto: number;
  idBandeira: number;

  constructor(
    private database: CombustivelDAO
  ) { }

  post(linhasArquivo: string[]) {
    // linhasArquivo.forEach(linha => {
    //   const linhaSplit = linha.split(ConstantsUtil.DIVISAO_COLUNAS_ARQUIVO);
    //   this.tratarEndereco(linhaSplit);
    // });

    for (let index = 0; index < 100; index++) {
      const linhaSplit = linhasArquivo[index].split(ConstantsUtil.DIVISAO_COLUNAS_ARQUIVO);
      this.tratarEndereco(linhaSplit);
    }

    return from(this.database.insertArquivo(linhasArquivo));
  }

  private tratarEndereco(linhaSplit: string[]) {
    const regiao = linhaSplit[ConstantsUtil.POSICAO_ENDERECO_REGIAO];
    const sigla = linhaSplit[ConstantsUtil.POSICAO_ENDERECO_SIGLA];
    const municipio = linhaSplit[ConstantsUtil.POSICAO_ENDERECO_MUNICIPIO];

    from(this.database.selectEnderecoByMunicipio(municipio)).
      subscribe((response) => {
        if (!response.payload || response.payload === null || response.payload === undefined) {
          from(this.database.insertEndereco({
            municipio,
            regiao,
            sigla
          })).subscribe(
            (res) => {
              this.idEndereco = res.payload;
            }
          );
        }
        if (response.payload && response.payload.id) {
          this.idEndereco = response.payload.id;
        }
        this.tratarRevendedor(linhaSplit);
      });
  }

  private tratarRevendedor(linhaSplit: string[]) {
    const descricao = linhaSplit[ConstantsUtil.POSICAO_REVENDEDOR];

    from(this.database.selectRevendedor(descricao)).
      subscribe((response) => {
        if (!response.payload || response.payload === null || response.payload === undefined) {
          from(this.database.insertRevendedor(descricao)).subscribe(
            (res) => {
              this.idRevendedor = res.payload;
            }
          );
        }
        if (response.payload && response.payload.id) {
          this.idRevendedor = response.payload.id;
        }
        this.tratarProduto(linhaSplit);
      });
  }

  private tratarProduto(linhaSplit: string[]) {
    const descricao = linhaSplit[ConstantsUtil.POSICAO_DESCRICAO_PRODUTO];

    from(this.database.selectProduto(descricao)).
      subscribe((response) => {
        if (!response.payload || response.payload === null || response.payload === undefined) {
          from(this.database.insertProduto(descricao)).subscribe(
            (res) => {
              this.idProduto = res.payload;
            }
          );
        }
        if (response.payload && response.payload.id) {
          this.idProduto = response.payload.id;
        }
        this.tratarBandeira(linhaSplit);
      });
  }

  private tratarBandeira(linhaSplit: string[]) {
    const descricao = linhaSplit[ConstantsUtil.POSICAO_BANDEIRA];

    from(this.database.selectBandeira(descricao)).
      subscribe((response) => {
        if (!response.payload || response.payload === null || response.payload === undefined) {
          from(this.database.insertBandeira(descricao)).subscribe(
            (res) => {
              this.idBandeira = res.payload;
              this.tratarCombustivel(linhaSplit);
            }
          );
        } else {
          this.idBandeira = response.payload.id;
          this.tratarCombustivel(linhaSplit);
        }
      });
  }

  private tratarCombustivel(linhaSplit: string[]) {
    const combustivel: CombustivelModel = {
      bandeira: this.idBandeira,
      codigo: linhaSplit[ConstantsUtil.POSICAO_CODIGO_COMBUSTIVEL],
      data: linhaSplit[ConstantsUtil.POSICAO_DATA],
      endereco: this.idEndereco,
      produto: this.idProduto,
      revendedor: this.idRevendedor,
      unidadeMedida: linhaSplit[ConstantsUtil.POSICAO_UNIDADE_MEDIDA],
      valorCompra: linhaSplit[ConstantsUtil.POSICAO_VALOR_COMPRA],
      valorVenda: linhaSplit[ConstantsUtil.POSICAO_VALOR_VENDA]
    };

    return from(this.database.insertCombustivel(combustivel));
  }

  getAll(pagina: number, ordenarRevendedor: boolean): Observable<RetornoPadraoModel<PaginacaoModel<CombustivelModel[]>>> {
    return from(this.database.selectCombustiveis(pagina, ordenarRevendedor));
  }

  getAllEnderecos() {
    return from(this.database.selectAllEnderecos());
  }

  calcularMediaCombustivelPorMunicipio(idMunicipio: number) {
    return from(this.database.calcularMediaValorCombustivel(idMunicipio));
  }

}
