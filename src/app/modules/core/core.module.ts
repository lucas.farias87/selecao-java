import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiUsuarioService } from './api-usuario/api-usuario.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from './interceptor/interceptor.service';
import { AuthenticationService } from './authentication/authentication.service';
import { ApiCombustivelService } from './api-combustivel/api-combustivel.service';
import { UsuarioDAO } from './database/usuario.dao.service';
import { DatabaseConnectionService } from './database/database-connection.service';
import { CombustivelDAO } from './database/combustivel.dao.service';

@NgModule({
  imports: [CommonModule],
  exports: [CommonModule]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        DatabaseConnectionService,
        UsuarioDAO,
        CombustivelDAO,
        ApiUsuarioService,
        ApiCombustivelService,
        AuthenticationService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: InterceptorService,
          multi: true
        }
      ]
    };
  }
}
