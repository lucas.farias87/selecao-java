import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { UsuarioModel } from '../../public/modules/usuarios/models/usuario.model';
import { RetornoPadraoModel } from '../../shared/models/retorno-padrao.model';
import { UsuarioDAO } from '../database/usuario.dao.service';

@Injectable()
export class ApiUsuarioService {

  constructor(
    private database: UsuarioDAO
  ) { }

  post(usuario: UsuarioModel): Observable<RetornoPadraoModel<number>> {
    return from(this.database.insertUsuario(usuario));
  }

  put(usuario: UsuarioModel): Observable<RetornoPadraoModel<number>> {
    return from(this.database.updateUsuario(usuario));
  }

  delete(id: number): Observable<RetornoPadraoModel<number>> {
    return from(this.database.deleteUsuario(id));
  }

  getAll(): Observable<RetornoPadraoModel<UsuarioModel[]>> {
    return from(this.database.selectUsuario());
  }

  getById(id: number): Observable<RetornoPadraoModel<UsuarioModel>> {
    return from(this.database.selectUsuarioById(id));
  }
}
