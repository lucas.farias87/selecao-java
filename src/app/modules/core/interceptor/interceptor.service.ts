import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable()
export class InterceptorService implements HttpInterceptor {

  constructor(
    private authenticationService: AuthenticationService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let reqToken = null;
    if (this.authenticationService.isLogged()) {
      reqToken = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${this.authenticationService.getUsuarioLogado().token}`)
      });
    }
    return next.handle(reqToken !== null ? reqToken : req).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
      } else {
      }
    },
      (err: any) => {
      }));
  }
}
