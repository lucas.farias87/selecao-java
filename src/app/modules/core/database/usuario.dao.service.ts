import { Injectable } from '@angular/core';
import { UsuarioModel } from '../../public/modules/usuarios/models/usuario.model';
import { MensagemEnum } from '../../shared/models/mensagem.enum';
import { RetornoPadraoModel } from '../../shared/models/retorno-padrao.model';
import { DatabaseConnectionService } from './database-connection.service';

@Injectable()
export class UsuarioDAO {

  constructor(
    private databaseConnectionService: DatabaseConnectionService
  ) { }

  public criarTabelas() {
    this.criarTabelaUsuarios();
  }

  private criarTabelaUsuarios() {
    this.databaseConnectionService.db.transaction((transaction) => {
      transaction.executeSql('CREATE TABLE IF NOT EXISTS usuario (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, nome TEXT NOT NULL, telefone TEXT NOT NULL, email TEXT NOT NULL)');
    });
  }

  insertUsuario(usuario: UsuarioModel) {
    return new Promise<RetornoPadraoModel<number>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('INSERT INTO usuario (nome, telefone, email) VALUES (?,?,?)',
          [usuario.nome, usuario.telefone, usuario.email],
          (tx, result) => {
            resolve({
              payload: result.insertId,
              mensagem: MensagemEnum.DADOS_SALVO_COM_SUCESSO
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CADASTRAR.replace('{0}', 'usuário')
            });
          }
        );
      });
    });
  }

  updateUsuario(usuario: UsuarioModel) {
    return new Promise<RetornoPadraoModel<number>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('UPDATE usuario SET nome = ?, telefone = ?, email = ? WHERE id = ?',
          [usuario.nome, usuario.telefone, usuario.email, usuario.id],
          () => {
            resolve({
              payload: usuario.id,
              mensagem: MensagemEnum.DADOS_SALVO_COM_SUCESSO
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_EDITAR.replace('{0}', 'usuário')
            });
          }
        );
      });
    });
  }

  deleteUsuario(id: number) {
    return new Promise<RetornoPadraoModel<number>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('DELETE FROM usuario WHERE id = ?',
          [id],
          () => {
            resolve({
              payload: id,
              mensagem: MensagemEnum.DADOS_REMOVIDOS_COM_SUCESSO
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_REMOVER.replace('{0}', 'usuário')
            });
          }
        );
      });
    });
  }

  selectUsuario() {
    const usuarios: UsuarioModel[] = [];
    return new Promise<RetornoPadraoModel<UsuarioModel[]>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('SELECT * FROM usuario',
          [],
          (tx, result) => {
            for (let index = 0; index < result.rows.length; index++) {
              usuarios.push(result.rows.item(index));
            }
            resolve({
              payload: usuarios
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CONSULTAR_BASE
            });
          });
      });
    });
  }

  selectUsuarioById(id: number) {
    let usuario: UsuarioModel;
    return new Promise<RetornoPadraoModel<UsuarioModel>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('SELECT * FROM usuario WHERE id = ?',
          [id],
          (tx, result) => {
            for (let index = 0; index < result.rows.length; index++) {
              usuario = result.rows.item(index);
            }
            resolve({
              payload: usuario
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CONSULTAR_BASE
            });
          });
      });
    });
  }

}
