import { Injectable } from '@angular/core';
import { MensagemEnum } from '../../shared/models/mensagem.enum';

@Injectable()
export class DatabaseConnectionService {

  db: any;

  constructor() { }

  criarDatabase() {
    this.db = (window as any).openDatabase('database', '1.0', 'database desafio', 2 * 1024 * 1024);

    if (!this.db || this.db === null || this.db === undefined) {
      throw new Error(MensagemEnum.ERRO_CRIACAO_DATABASE);
    }
  }
}
