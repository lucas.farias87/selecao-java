import { Injectable } from '@angular/core';
import { EnderecoModel } from '../../public/modules/combustiveis/models/endereco.model';
import { RevendedorModel } from '../../public/modules/combustiveis/models/revendedor.model';
import { MensagemEnum } from '../../shared/models/mensagem.enum';
import { RetornoPadraoModel } from '../../shared/models/retorno-padrao.model';
import { DatabaseConnectionService } from './database-connection.service';
import { ProdutoModel } from '../../public/modules/combustiveis/models/produto.model';
import { BandeiraModel } from '../../public/modules/combustiveis/models/bandeira.model';
import { CombustivelModel } from '../../public/modules/combustiveis/models/combustivel.model';
import { ConstantsUtil } from '../../shared/util/constants-util.const';
import { PaginacaoModel } from '../../shared/models/paginacao.model';

@Injectable()
export class CombustivelDAO {

  constructor(
    private databaseConnectionService: DatabaseConnectionService
  ) { }

  public criarTabelas() {
    this.criarTabelaEndereco();
  }

  private criarTabelaCombustivel() {
    this.databaseConnectionService.db.transaction((transaction) => {
      transaction.executeSql(
        'CREATE TABLE IF NOT EXISTS combustivel (' +
        'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
        'fk_endereco INTEGER,' +
        'fk_revendedor INTERGER,' +
        'codigo TEXT,' +
        'fk_produto INTERGER,' +
        'data TEXT,' +
        'valor_compra DOUBLE,' +
        'valor_venda DOUBLE,' +
        'unidade_medida TEXT,' +
        'fk_bandeira INTEGER,' +
        'FOREIGN KEY (fk_endereco) REFERENCES endereco(id),' +
        'FOREIGN KEY (fk_revendedor) REFERENCES revendedor(id),' +
        'FOREIGN KEY (fk_produto) REFERENCES produto(id),' +
        'FOREIGN KEY (fk_bandeira) REFERENCES bandeira(id))'
      );
    });
  }

  private criarTabelaEndereco() {
    this.databaseConnectionService.db.transaction((transaction) => {
      transaction.executeSql(
        'CREATE TABLE IF NOT EXISTS endereco (' +
        'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
        'regiao TEXT,' +
        'sigla TEXT,' +
        'municipio TEXT UNIQUE)',
        [],
        () => {
          this.criarTabelaRevendedor();
        }
      );
    });
  }

  private criarTabelaRevendedor() {
    this.databaseConnectionService.db.transaction((transaction) => {
      transaction.executeSql(
        'CREATE TABLE IF NOT EXISTS revendedor (' +
        'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
        'descricao TEXT UNIQUE)',
        [],
        () => {
          this.criarTabelaProduto();
        }
      );
    });
  }

  private criarTabelaProduto() {
    this.databaseConnectionService.db.transaction((transaction) => {
      transaction.executeSql(
        'CREATE TABLE IF NOT EXISTS produto (' +
        'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
        'descricao TEXT UNIQUE)',
        [],
        () => {
          this.criarTabelaBandeira();
        }
      );
    });
  }

  private criarTabelaBandeira() {
    this.databaseConnectionService.db.transaction((transaction) => {
      transaction.executeSql(
        'CREATE TABLE IF NOT EXISTS bandeira (' +
        'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
        'descricao TEXT UNIQUE)',
        [],
        () => {
          this.criarTabelaCombustivel();
        }
      );
    });
  }

  insertArquivo(linhasArquivo: string[]) {
    return new Promise<RetornoPadraoModel<number>>((resolve, reject) => {
    });
  }

  selectEnderecoByMunicipio(municipio: string) {
    let endereco: EnderecoModel;
    return new Promise<RetornoPadraoModel<EnderecoModel>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('SELECT * FROM endereco WHERE municipio = ?',
          [municipio],
          (tx, result) => {
            for (let index = 0; index < result.rows.length; index++) {
              endereco = result.rows.item(index);
            }
            resolve({
              payload: endereco
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CONSULTAR_BASE
            });
          });
      });
    });
  }

  selectAllEnderecos() {
    const endereco: EnderecoModel[] = [];
    return new Promise<RetornoPadraoModel<EnderecoModel[]>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('SELECT * FROM endereco',
          [],
          (tx, result) => {
            for (let index = 0; index < result.rows.length; index++) {
              endereco.push(result.rows.item(index));
            }
            resolve({
              payload: endereco
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CONSULTAR_BASE
            });
          });
      });
    });
  }

  insertEndereco(endereco: EnderecoModel) {
    return new Promise<RetornoPadraoModel<number>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('INSERT INTO endereco (regiao, sigla, municipio) VALUES (?,?,?)',
          [endereco.regiao, endereco.sigla, endereco.municipio],
          (tx, result) => {
            resolve({
              payload: result.insertId,
              mensagem: MensagemEnum.DADOS_SALVO_COM_SUCESSO
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CADASTRAR.replace('{0}', 'endereço')
            });
          }
        );
      });
    });
  }

  selectRevendedor(descricao: string) {
    let revendedor: RevendedorModel;
    return new Promise<RetornoPadraoModel<RevendedorModel>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('SELECT * FROM revendedor WHERE descricao = ?',
          [descricao],
          (tx, result) => {
            for (let index = 0; index < result.rows.length; index++) {
              revendedor = result.rows.item(index);
            }
            resolve({
              payload: revendedor
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CONSULTAR_BASE
            });
          });
      });
    });
  }

  insertRevendedor(descricao: string) {
    return new Promise<RetornoPadraoModel<number>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('INSERT INTO revendedor (descricao) VALUES (?)',
          [descricao],
          (tx, result) => {
            resolve({
              payload: result.insertId,
              mensagem: MensagemEnum.DADOS_SALVO_COM_SUCESSO
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CADASTRAR.replace('{0}', 'revendedor')
            });
          }
        );
      });
    });
  }

  selectProduto(descricao: string) {
    let produto: ProdutoModel;
    return new Promise<RetornoPadraoModel<ProdutoModel>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('SELECT * FROM produto WHERE descricao = ?',
          [descricao],
          (tx, result) => {
            for (let index = 0; index < result.rows.length; index++) {
              produto = result.rows.item(index);
            }
            resolve({
              payload: produto
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CONSULTAR_BASE
            });
          });
      });
    });
  }

  insertProduto(descricao: string) {
    return new Promise<RetornoPadraoModel<number>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('INSERT INTO produto (descricao) VALUES (?)',
          [descricao],
          (tx, result) => {
            resolve({
              payload: result.insertId,
              mensagem: MensagemEnum.DADOS_SALVO_COM_SUCESSO
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CADASTRAR.replace('{0}', 'produto')
            });
          }
        );
      });
    });
  }

  selectBandeira(descricao: string) {
    let produto: BandeiraModel;
    return new Promise<RetornoPadraoModel<BandeiraModel>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('SELECT * FROM bandeira WHERE descricao = ?',
          [descricao],
          (tx, result) => {
            for (let index = 0; index < result.rows.length; index++) {
              produto = result.rows.item(index);
            }
            resolve({
              payload: produto
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CONSULTAR_BASE
            });
          });
      });
    });
  }

  insertBandeira(descricao: string) {
    return new Promise<RetornoPadraoModel<number>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('INSERT INTO bandeira (descricao) VALUES (?)',
          [descricao],
          (tx, result) => {
            resolve({
              payload: result.insertId,
              mensagem: MensagemEnum.DADOS_SALVO_COM_SUCESSO
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CADASTRAR.replace('{0}', 'bandeira')
            });
          }
        );
      });
    });
  }

  selectCombustivel(codigo: string) {
    let combustivel: CombustivelModel;
    return new Promise<RetornoPadraoModel<CombustivelModel>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('SELECT * FROM combustivel WHERE codigo = ?',
          [codigo],
          (tx, result) => {
            for (let index = 0; index < result.rows.length; index++) {
              combustivel = result.rows.item(index);
            }
            resolve({
              payload: combustivel
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CONSULTAR_BASE
            });
          });
      });
    });
  }

  insertCombustivel(combustivel: CombustivelModel) {
    return new Promise<RetornoPadraoModel<number>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql(
          'INSERT INTO combustivel (fk_endereco, fk_revendedor, codigo, fk_produto, data, valor_compra, valor_venda, unidade_medida, fk_bandeira)' +
          'VALUES (?,?,?,?,?,?,?,?,?)',
          // tslint:disable-next-line: max-line-length
          [combustivel.endereco, combustivel.revendedor, combustivel.codigo, combustivel.produto, combustivel.data, String(combustivel.valorCompra).replace(',', '.'), String(combustivel.valorVenda).replace(',', '.'), combustivel.unidadeMedida, combustivel.bandeira],
          (tx, result) => {
            resolve({
              payload: result.insertId,
              mensagem: MensagemEnum.DADOS_SALVO_COM_SUCESSO
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CADASTRAR.replace('{0}', 'combustivel')
            });
          }
        );
      });
    });
  }

  selectCombustiveis(pagina: number, orderRevendedor = false) {
    const combustiveis: CombustivelModel[] = [];
    let totalRegistro: number;
    const queryOrderByRevendedor = 'SELECT c.id, e.regiao, e.sigla, e.municipio, r.descricao AS desc_rev, c.codigo, p.descricao AS desc_prod, c.data, c.valor_compra, c.valor_venda, c.unidade_medida, b.descricao AS desc_band ' +
      'FROM endereco e, revendedor r, combustivel c, produto p, bandeira b ' +
      'WHERE c.fk_endereco = e.id AND c.fk_revendedor = r.id AND c.fk_produto = p.id AND c.fk_bandeira = b.id ORDER BY r.descricao LIMIT ?  OFFSET (? - 1) * ?';

    const queryGetAll = 'SELECT c.id, e.regiao, e.sigla, e.municipio, r.descricao AS desc_rev, c.codigo, p.descricao AS desc_prod, c.data, c.valor_compra, c.valor_venda, c.unidade_medida, b.descricao AS desc_band ' +
      'FROM endereco e, revendedor r, combustivel c, produto p, bandeira b ' +
      'WHERE c.fk_endereco = e.id AND c.fk_revendedor = r.id AND c.fk_produto = p.id AND c.fk_bandeira = b.id LIMIT ?  OFFSET (? - 1) * ?';


    return new Promise<RetornoPadraoModel<PaginacaoModel<CombustivelModel[]>>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql(
          'SELECT COUNT(1) AS total_comb ' +
          'FROM combustivel',
          [],
          (tx, result) => {
            totalRegistro = result.rows[0].total_comb;
          });
      });

      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql(orderRevendedor ? queryOrderByRevendedor : queryGetAll,
          [ConstantsUtil.LIMITE_ITENS_PAGINA, pagina, ConstantsUtil.LIMITE_ITENS_PAGINA],
          (tx, result) => {
            for (let index = 0; index < result.rows.length; index++) {
              const combustivel: CombustivelModel = {
                id: result.rows.item(index).id,
                endereco: {
                  regiao: result.rows.item(index).regiao,
                  municipio: result.rows.item(index).municipio,
                  sigla: result.rows.item(index).sigla
                },
                revendedor: {
                  descricao: result.rows.item(index).desc_rev
                },
                produto: {
                  descricao: result.rows.item(index).desc_prod
                },
                bandeira: {
                  descricao: result.rows.item(index).desc_band
                },
                codigo: result.rows.item(index).codigo,
                data: result.rows.item(index).data,
                unidadeMedida: result.rows.item(index).unidade_medida,
                valorCompra: result.rows.item(index).valor_compra,
                valorVenda: result.rows.item(index).valor_venda
              };
              combustiveis.push(combustivel);
            }
            resolve({
              payload: {
                itens: combustiveis,
                pagina,
                totalItens: totalRegistro
              }
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CONSULTAR_BASE
            });
          });
      });
    });
  }

  calcularMediaValorCombustivel(idMunicipio: number) {
    let media: number;
    return new Promise<RetornoPadraoModel<number>>((resolve, reject) => {
      this.databaseConnectionService.db.transaction((transaction) => {
        transaction.executeSql('SELECT AVG(c.valor_venda) AS media_valor FROM combustivel c, endereco e WHERE e.id = ? AND c.fk_endereco = e.id',
          [idMunicipio],
          (tx, result) => {
            for (let index = 0; index < result.rows.length; index++) {
              media = result.rows.item(index).media_valor;
            }
            resolve({
              payload: media
            });
          },
          () => {
            reject({
              mensagem: MensagemEnum.ERRO_CONSULTAR_BASE
            });
          });
      });
    });
  }
}
